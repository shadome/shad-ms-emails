source .docker.env

docker build --network host -t $OS_IMAGE -f dockerfile-webapp-os.dockerfile ..
docker build --network host -t $SRC_IMAGE --build-arg OS_IMAGE=$OS_IMAGE -f dockerfile-webapp-src.dockerfile ..

docker run --rm -dit \
    --net=host \
    --name ${TEST_CONTAINER} \
    $SRC_IMAGE

docker exec -it ${TEST_CONTAINER} cargo test --release -- --nocapture --test-threads 1 --quiet || true

docker container rm -f ${TEST_CONTAINER}
docker image prune -f
