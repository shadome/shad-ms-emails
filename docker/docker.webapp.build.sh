source .docker.env

docker build --network host -t $OS_IMAGE -f dockerfile-webapp-os.dockerfile ..
docker build --network host -t $SRC_IMAGE --build-arg OS_IMAGE=$OS_IMAGE -f dockerfile-webapp-src.dockerfile ..
docker build --network host -t $WS_IMAGE --build-arg SRC_IMAGE=$SRC_IMAGE -f dockerfile-webapp-ws.dockerfile ..

docker image prune -f
