source .docker.env

# Note: 
#   Doesn't seem to work with `-h host.docker.internal` and `-p X:Y`,
#   I don't know why

docker container rm -f ${WS_CONTAINER} || true

docker run --rm -dit \
    --net=host \
    --name ${WS_CONTAINER} \
    ${WS_IMAGE}

docker image prune -f