# Note that cargo build, using sqlx, will require an up-to-date and running postgresql instance,
# and the database url (with password and all) which is why the .env file is copied

ARG OS_IMAGE
FROM $OS_IMAGE

###
# Build the rust application
###
WORKDIR /app

# Create an empty dummy project and build it to cache the release dependencies
# See https://stackoverflow.com/questions/58473606/cache-rust-dependencies-with-docker-build

# Copy only the dependency manifests first to leverage Docker caching
COPY Cargo.toml Cargo.lock .env ./
# Copy the dummy main.rs replacement and temporarily modify the Cargo.toml to reference it
COPY docker/docker_build_dependencies.rs ./
RUN sed -i 's#src/main.rs#docker_build_dependencies.rs#' Cargo.toml
# Build project dependencies
RUN cargo build --release
# Restore the Cargo.toml
RUN sed -i 's#docker_build_dependencies.rs#src/main.rs#' Cargo.toml

# Compile the actual source code files which might have been modified
COPY src ./src
RUN cargo build --release
