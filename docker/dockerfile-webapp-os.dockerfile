# https://hub.docker.com/_/archlinux/tags
FROM archlinux:base-20230611.0.157136

RUN pacman -Sy --noconfirm rustup clang openssl pkg-config
RUN rustup default stable
RUN rustup update
