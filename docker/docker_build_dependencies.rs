// This file is used by docker so that project dependencies can be built independently of the source code and then cached
// so that successive rebuilds only take the source code changes into account
fn main() {}