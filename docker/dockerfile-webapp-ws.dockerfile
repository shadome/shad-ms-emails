ARG SRC_IMAGE
FROM $SRC_IMAGE

EXPOSE 8070
CMD ["./target/release/shad-ms-emails"]
