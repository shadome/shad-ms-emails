
pub(crate) async fn normalise(email_address: &str) -> Result<String, crate::AppError> {
    let (mut local, mut domain) = get_local_and_domain(email_address)?;

    let rules = if let Some(provider) = get_best_host_mx_name(&domain).await? {
        provider.rules
    } else {
        // Default rules when the email provider is not mapped in this code.
        // Note that those rules can lead to both false positive and false negative
        // email address uniqueness, as the actual rules are only known to the
        // email provider and could evolve in time.
        vec![ Rules::StripPeriods, Rules::PlusAddressing ]
    };

    if rules.contains(&Rules::LocalPartAsHostName) {
        (local, domain) = get_local_part_as_hostname(&local, &domain);
    }

    if rules.contains(&Rules::DashAddressing) {
        let local_parts = local.split('-').collect::<Vec<_>>();
        if let Some(lp) = local_parts.first() {
            local = lp.to_string();
        }
    }

    if rules.contains(&Rules::PlusAddressing) {
        let local_parts = local.split('+').collect::<Vec<_>>();
        if let Some(lp) = local_parts.first() {
            local = lp.to_string();
        }
    }

    if rules.contains(&Rules::StripPeriods) {
        let new_local = local.replace(".", "");
        local = new_local;
    }

    let normalized_address = format!("{}@{}", local, domain);
    Result::Ok(normalized_address)
}

// private

#[repr(u16)]
#[derive(Eq, PartialEq, Debug, Copy, Clone)]
enum Rules {
    DashAddressing,
    PlusAddressing,
    LocalPartAsHostName,
    StripPeriods,
}

#[derive(Clone, Debug, PartialEq, Eq)]
struct MappedProvider {
    pub(super) rules: Vec<Rules>,
    pub(super) mx_domains: Vec<String>,
    pub(super) name: String,
}

fn get_local_and_domain(email_address: &str) -> Result<(String, String), crate::AppError> {
    let parts = email_address.split('@').collect::<Vec<_>>();
    if parts.len() != 2 || parts[0].is_empty() || parts[1].is_empty() {
        Err(crate::AppError::new("Invalid email format".to_string(), crate::ServerStatusCode::InvalidFormat))
    } else {
        Result::Ok((
            parts[0].to_string().to_lowercase(),
            parts[1].to_string().to_lowercase(),
        ))
    }
}

async fn get_best_host_mx_name(domain_name: &str) -> Result<Option<MappedProvider>, crate::AppError> {
    let best_mx_host = get_resolver()
        .mx_lookup(domain_name)
        .await
        .map_err(|err| crate::AppError {
            message: format!("The provided email does not exist.\nDetailed error: {}", err.to_string()),
            http_status_code: actix_web::http::StatusCode::BAD_REQUEST,
            server_status_code: crate::ServerStatusCode::DnsLookupError,
        })?
        .iter()
        .min_by_key(|mx_record| mx_record.preference())
        .and_then(|mx_record| Some(mx_record.exchange().to_string()));

    if let Some(best_mx_host) = best_mx_host {
        let providers = get_providers();
        for provider in providers.iter() {
            for domain in provider.mx_domains.iter() {
                let doted_domain = format!("{}{}", domain, '.');
                if best_mx_host.ends_with(&doted_domain) {
                    return Ok(Some(provider.clone()));
                }
            }
        }
    }
    return Ok(None);

    fn get_resolver() -> trust_dns_resolver::TokioAsyncResolver {
        let mut opts = trust_dns_resolver::config::ResolverOpts::default();
        opts.ndots = 0;
        trust_dns_resolver::TokioAsyncResolver::tokio(trust_dns_resolver::config::ResolverConfig::google(), opts).unwrap()
    }
    
    fn get_providers() -> Vec<MappedProvider> {
        vec![
            MappedProvider {
                rules: vec![Rules::PlusAddressing],
                mx_domains: vec![String::from("icloud.com")],
                name: String::from("APPLE")
            },
            MappedProvider {
                rules: vec![Rules::PlusAddressing, Rules::LocalPartAsHostName],
                mx_domains: vec![String::from("messagingengine.com")],
                name: String::from("FAST_MAIL")
            },
            MappedProvider {
                rules: vec![Rules::PlusAddressing, Rules::StripPeriods],
                mx_domains: vec![String::from("google.com")],
                name: String::from("GOOGLE")
            },
            MappedProvider {
                rules: vec![Rules::PlusAddressing],
                mx_domains: vec![String::from("outlook.com")],
                name: String::from("MICROSOFT")
            },
            MappedProvider {
                rules: vec![Rules::PlusAddressing],
                mx_domains: vec![String::from("protonmail.ch")],
                name: String::from("PROTONMAIL")
            },
            MappedProvider {
                rules: vec![Rules::PlusAddressing],
                mx_domains: vec![String::from("emailsrvr.com")],
                name: String::from("RACKSPACE")
            },
            MappedProvider {
                rules: vec![Rules::DashAddressing],
                mx_domains: vec![String::from("yahoodns.net")],
                name: String::from("YAHOO")
            },
            MappedProvider {
                rules: vec![Rules::PlusAddressing],
                mx_domains: vec![String::from("mx.yandex.net"), String::from("yandex.ru")],
                name: String::from("YANDEX")
            },
            MappedProvider {
                rules: vec![Rules::PlusAddressing],
                mx_domains: vec![String::from("zoho.com")],
                name: String::from("ZOHOO")
            }
        ]
    }
    
}

fn get_local_part_as_hostname(local_part: &str, domain_part: &str) -> (String, String) {
    let mut local_part_inner = local_part.to_string();
    let mut domain_part_inner = domain_part.to_string();
    let domain_splits = domain_part.split('.').collect::<Vec<_>>();
    if domain_splits.len() > 2 {
        local_part_inner = domain_splits[0].to_string();
        domain_part_inner = domain_splits[1..].join(".");
    }

    (local_part_inner, domain_part_inner)
}

// ---

// Note: the following code is a tentative at implementing the checking and normalisation
// offline, i.e., without using an MX provider / a DNS lookup.
// It has been aborted, probably when realising that beyond the global RFC common rules,
// the exact email naming restrictions are a choice enforced by the actual email provider.
// This code is not used as of 23/01/2024 but should not be deleted yet.

// ---

// pub(crate) struct NormalisedEmailAddress {
//     raw_email_addr: String,
//     normalised_email_addr: String,
//     local_part: String,
//     domain_part: String,
// }

// impl NormalisedEmailAddress {
//     pub(self) fn new(raw_email_addr: &str, local_part: &str, domain_part: &str) -> Self {
//         Self {
//             raw_email_addr: raw_email_addr.to_string(),
//             local_part: local_part.to_string(),
//             domain_part: domain_part.to_string(),
//             normalised_email_addr: format!("{}@{}", local_part, domain_part),
//         }
//     }
// }


// /// Applies checks and normalisation steps considered independant of the MX provider. <br/>
// /// It basically consists in normalising to lowercase and checkings a bunch of RFC rules. <br/>
// /// Things such as removing periods or tags are conditional to the MX provider. <br/>
// /// Constraints sources:
// /// * https://www.mailboxvalidator.com/resources/articles/acceptable-email-address-syntax-rfc/
// /// * chatgpt
// pub(self) fn check_and_normalise_offline(raw_email_addr: &str) -> Result<NormalisedEmailAddress, crate::AppError> {
//     let email_addr = raw_email_addr.to_ascii_lowercase();
//     let (local_part, domain_part) = get_local_and_domain(&email_addr)?;

//     check_characters(&local_part, &domain_part)?;
//     check_ends_and_consecutive_chars(&local_part, &domain_part)?;
//     check_lengths(&local_part, &domain_part)?;

//     let normalised_email_addr = NormalisedEmailAddress::new(raw_email_addr, &local_part, &domain_part);

//     Ok(normalised_email_addr)
// }

// /// Enforces global constraints on the raw email:
// /// * Maximum length of the local part is 64 characters.
// /// * Maximum length of the domain part is 255 characters.
// /// * Maximum length of each domain label (between periods) is 63 characters.
// /// * Maximum number of domain labels is 127.
// /// * Top level domain cannot be all numeric characters.
// fn check_lengths(local: &str, domain: &str) -> Result<(), crate::AppError> {
//     let domain_labels = domain.split('.').collect::<Vec<&str>>();
//     let is_in_error = false
//         || local.len() > 64
//         || domain.len() > 255
//         || domain_labels.len() > 127
//         || domain_labels.iter().any(|label| label.len() > 63)
//         || domain_labels.last().unwrap().chars().all(|c| c.is_ascii_digit())
//     ;
//     if is_in_error {
//         let error = crate::AppError::new(
//             "Invalid email format".to_string(),
//             crate::ServerStatusCode::InvalidFormat
//         ); 
//         return Err(error);
//     }
//     Ok(())
// }

// /// Enforces that the raw email is composed of either:
// /// * uppercase and lowercase Latin letters A to Z and a to z
// /// * digits 0 to 9
// /// * for the local part: printable characters !#$%&'*+-/=?^_`{|}~.
// /// * for the domain part: printable characters -.
// fn check_characters(local: &str, domain: &str) -> Result<(), crate::AppError> {
//     let parts_and_eligible_chars = vec![
//         (local, "!#$%&'*+-/=?^_`{|}~."),
//         (domain, "-.")
//     ];
//     for (part, eligible_chars) in parts_and_eligible_chars {
//         let all_characters_are_eligible = part
//             .chars()
//             .all(|c| (c.is_ascii_alphanumeric() || eligible_chars.contains(c)));
        
//         if !all_characters_are_eligible {
//             let error = crate::AppError::new(
//                 "Invalid email format".to_string(),
//                 crate::ServerStatusCode::InvalidFormat
//             ); 
//             return Err(error);
//         }
//     }
//     Ok(())
// }

// /// Enforces that the raw email:
// /// * for the local part, cannot start or end with a period (.).
// /// * for the domain part, cannot start or end with a hyphen (-).
// /// * periods are allowed within the local part but not consecutively (e.g., "john..doe" is invalid).
// fn check_ends_and_consecutive_chars(local: &str, domain: &str) -> Result<(), crate::AppError> {
//     let parts_and_ineligible_ends_and_can_be_consecutive = vec![
//         (local, b'.', false),
//         (domain, b'-', true),
//     ];
//     for (input, byte, can_be_consecutive) in parts_and_ineligible_ends_and_can_be_consecutive {
//         let bytes = input.as_bytes();
//         let len = bytes.len();

//         if len == 0 || bytes[0] == byte || bytes[len - 1] == byte {
//             let error = crate::AppError::new(
//                 "Invalid email format.".to_string(),
//                 crate::ServerStatusCode::InvalidFormat
//             ); 
//             return Err(error);
//         }
//         if !can_be_consecutive {
//             let mut prev_char = b'\0';
//             for &byte in bytes {
//                 if byte == byte && prev_char == byte {
//                     let error = crate::AppError::new(
//                         "Invalid email format".to_string(),
//                         crate::ServerStatusCode::InvalidFormat
//                     ); 
//                     return Err(error);
//                 }
//                 prev_char = byte;
//             }
//         }
//     }

//     Ok(())
// }
