# Motivation

This module is a fork of https://crates.io/crates/norm-email using the `trust-dns-resolver` dependency with tokio runtime async support.

The `norm-mail` crate's implementation unconditionnally uses the synchronous version of `trust-dns-resolver`, which leads to the followig runtime error when used with `actix-web`:
> Cannot start a runtime from within a runtime. This happens because a function (like `block_on`) attempted to block the current thread while the thread is being used to drive asynchronous tasks.