#[cfg(test)]
mod tests;

pub(super) async fn route(
    url_email: actix_web::web::Path<String>
) -> Result<String, crate::AppError> {
    let email = url_email.into_inner();

    if !mailchecker::is_valid(&email) { 
        let err = crate::AppError {
            http_status_code: actix_web::http::StatusCode::BAD_REQUEST,
            server_status_code: crate::ServerStatusCode::InvalidFormatOrBlacklistedEmail,
            message: String::from("The provided email is invalid or blacklisted"),
        };
        return Err(err)
    }
    let normalised_email = crate::services::normaliser::normalise(&email).await?;

    Ok(normalised_email)
}
