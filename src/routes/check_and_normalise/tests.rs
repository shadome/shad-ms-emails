
// Note that neither OK nor KO tests are meant to extensively re-test
// the crates which are used, only a few happy / sad paths.

// Note that those tests require, for the most part, a working Internet connection for DNS lookups.
// This is not a problem now but mocks should be implemented to avoid actual DNS lookup.

#[actix_web::test]
async fn test_check_and_normalise_email_ok() {
    // arrange
    let email = String::from("toto@gmail.com");
    let expected = email.clone();
    // act
    let path = actix_web::web::Path::from(email);
    let result = super::route(path).await;
    // assert
    assert!(result.is_ok());
    assert_eq!(result.unwrap(), expected);
}

/// Check that the email subparts are correctly ignored when normalisation occurs
#[actix_web::test]
async fn test_check_and_normalise_email_ok_subparts() {
    // arrange
    let email = String::from("t.O.T.O+submailpart+anothersubmailpart@GMAIL.com");
    let expected = String::from("toto@gmail.com");
    // act
    let result = super::route(actix_web::web::Path::from(email)).await;
    // assert
    assert!(result.is_ok());
    assert_eq!(result.unwrap(), expected);
}

#[actix_web::test]
async fn test_check_and_normalise_email_no_dns_entry() {
    // arrange
    let email = String::from("toto@fasfjowerv.com");
    let expected_http_status_code = actix_web::http::StatusCode::BAD_REQUEST;
    let expected_server_status_code = crate::ServerStatusCode::DnsLookupError;
    // act
    let result = super::route(actix_web::web::Path::from(email)).await;
    // assert
    assert!(result.is_err());
    let crate::AppError { http_status_code: actual_http_status_code, server_status_code: actual_server_status_code, .. } = result.unwrap_err();
    assert_eq!(expected_http_status_code, actual_http_status_code);
    assert_eq!(expected_server_status_code, actual_server_status_code);
}

#[actix_web::test]
async fn test_check_and_normalise_email_unknown_dns_entry() {
    // arrange
    let email = String::from("researc.h+absurd@att.net");
    let expected = String::from("research@att.net");
    // act
    let result = super::route(actix_web::web::Path::from(email)).await;
    // assert
    assert!(result.is_ok());
    assert_eq!(result.unwrap(), expected);
}

/// Check that unknown providers are not refused by default, i.e.,
/// this function uses a blacklist, not a whitelist. <br/>
/// Note that this function tests the normaliser module when
/// no pre-registered DNS entry and rules match. <br/>
/// From <a>https://www.randomlists.com/email-addresses</a>
#[actix_web::test]
async fn test_check_and_normalise_email_ok_not_blacklisted() {
    // arrange
    let emails = vec![
        "choset@live.com",
        "arathi@icloud.com",
        "hamilton@msn.com",
        "mcast@hotmail.com",
        "gerlo@me.com",
        "calin@yahoo.com",
        "sblack@sbcglobal.net",
        "presoff@optonline.net",
        "bogjobber@gmail.com",
        "muadip@comcast.net",
        "duncand@outlook.com",
        "garland@aol.com",
        "zwood@mac.com",
        "johndoe@externe.e-i.com", // random french company domain
    ];
    // act, assert
    for email in emails {
        let result = super::route(actix_web::web::Path::from(String::from(email))).await;
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), email);
    }
}

#[actix_web::test]
async fn test_check_and_normalise_email_ko_invalid_1() {
    // arrange
    let email = String::from("toto_at_gmail.com");
    let expected_http_status_code = actix_web::http::StatusCode::BAD_REQUEST;
    let expected_server_status_code = crate::ServerStatusCode::InvalidFormatOrBlacklistedEmail;
    // act
    let result = super::route(actix_web::web::Path::from(email)).await;
    // assert
    assert!(result.is_err());
    let crate::AppError { http_status_code: actual_http_status_code, server_status_code: actual_server_status_code, .. } = result.unwrap_err();
    assert_eq!(expected_http_status_code, actual_http_status_code);
    assert_eq!(expected_server_status_code, actual_server_status_code);
}

#[actix_web::test]
async fn test_check_and_normalise_email_ko_invalid_2() {
    // arrange
    let email = String::from("toto@gmail_dot_com");
    let expected_http_status_code = actix_web::http::StatusCode::BAD_REQUEST;
    let expected_server_status_code = crate::ServerStatusCode::InvalidFormatOrBlacklistedEmail;
    // act
    let result = super::route(actix_web::web::Path::from(email)).await;
    // assert
    assert!(result.is_err());
    let crate::AppError { http_status_code: actual_http_status_code, server_status_code: actual_server_status_code, .. } = result.unwrap_err();
    assert_eq!(expected_http_status_code, actual_http_status_code);
    assert_eq!(expected_server_status_code, actual_server_status_code);
}

#[actix_web::test]
async fn test_check_and_normalise_email_ko_blacklisted() {
    // arrange
    let email = String::from("toto@yopmail.com");
    let expected_http_status_code = actix_web::http::StatusCode::BAD_REQUEST;
    let expected_server_status_code = crate::ServerStatusCode::InvalidFormatOrBlacklistedEmail;
    // act
    let result = super::route(actix_web::web::Path::from(email)).await;
    // assert
    assert!(result.is_err());
    let crate::AppError { http_status_code: actual_http_status_code, server_status_code: actual_server_status_code, .. } = result.unwrap_err();
    assert_eq!(expected_http_status_code, actual_http_status_code);
    assert_eq!(expected_server_status_code, actual_server_status_code);
}
