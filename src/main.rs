mod routes;
mod services;
mod utils;

use utils::*;

// Keep in sync with the content of the .env file
#[derive(Clone, Debug)]
pub struct Env {
    host: String,
    port: u16,
}

fn init_env_variables() -> Env {
    dotenvy::dotenv().ok();
    let host = std::env::var("HOST").expect("HOST env variable must be set");
    let port = std::env::var("PORT").expect("PORT env variable must be set").parse().expect("PORT env variable must be a i16");
    Env { host, port }
}

#[actix_web::main]
async fn main() -> Result<(), crate::AppError> {
    /* initialisation */
    // env variables
    let env = init_env_variables();
    /* application */
    routes::run_webapp(env).await?;
    Ok(())
}
