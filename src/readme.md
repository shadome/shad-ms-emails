# Testing another file structure

This code base aims at separating concerns by creating as many sub modules as required. Modules represent a scope, which is meant to expose code to some other parts of the application in a controlled fashion. In particular, the module hierarchy is to be conceived in the form of a dependency **tree** in most cases, keeping orthogonal layers *(ie., layers which can be access by multiple other layers with different levels of nesting)* to a minimum number.

As such, and considering Rust's module restrictions, the `mod.rs` syntax will be used to encourage folder nesting and to avoid duplicating module names *(e.g., having a duplicate `my_module.rs` file, containing the code, and a `my_module` folder, containing nested submodules)*.

Consequently, a module folder will typically contain:
- its `mod.rs` file, exposing at least one non-private function, struct, etc.
- an optional `tests.rs` file, containing tests private to the `mod.rs` code, if relevant *(not all layers are meant to be tested, in accordance with the TDD paradigm)*
- an optional `mod.drawio` if a diagram is provided to break down the conception of the functionalities provided by `mod.rs`
- markdown files, like `readme.md`, providing scope-level context information

# Fully qualified paths

Isn't it surprising to encounter fully qualified paths in the code, i.e., having no *use* statements at the start of a file? It should.

Fully qualified paths serve two purposes in this project:
- to distinguish easily between crate internal objects and external dependencies,
- to keep a permanent reminder of the potency of abstraction.

Neither Rust native libraries nor external crates should be used lightly. A good library provides powerful abstractions through its API, meaning that much should be done through a single line of code when using them.

As such, fully qualified names should act as a tiny thorn in the side and encourage to both minimise the number of calls to external code and regroup them in space (e.g., in a dedicated module).

Compared to other languages, the native libraries that Rust provide are a formidable exemple of well designed APIs and powerful abstractions, let's enjoy it to the fullest.